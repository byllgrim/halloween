# halloween

a Jamban new song before __halloween__?

Tivoli metal/speedcore/wtf?

Here's an "old" sketch.
https://gitlab.com/byllgrim/halloween/blob/master/current_render.opus

# Contributing

Tasks:
* Lyrics
  * https://github.com/byllgrim/jamban/blob/master/halloween.txt
* Composition
  * The song progression is freezed, to make colab possible
    * But chords, rhythms, etc are up for grabs!
  * Parts for new instruments
* Recording
  * Bass guitar
  * Backing vocals
  * Electric guitar
  * Lead vocals is reserved for the main jamalam
* Virtual instruments:
  * Drum samples
  * Orchestra soundfonts
  * Synths, FX, etc
* Processing
  * Mixing and Mastering
* Music video
  * Ideas
  * Actors
  * Editing
  * Drawings & animations
* Others
  * If you have any good ideas, I accepted almost unconditionally

Rabsi use linux so everything is weird.
You can either

* Share rendered audio files and we'll combine them
* Upload DAW project files in this repo
* Use Jack audio and the weird open source tools that rab likes

# File types in this repo

This is just the setup that I, rabanansan, initially started with.

Drum patterns are in `.h2song` for use with
http://hydrogen-music.org/screenshots/.

Melodic instruments are in `.mscx` for use with
https://musescore.org/en.

Audio system connecting everything:
http://jackaudio.org/
