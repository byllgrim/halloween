Simple Public License (SPL)
===========================
Permission is hereby granted to utilize this work in any way.

All rights are dedicated to the public domain.

THE WORK IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTY AND
LIABILITY.
