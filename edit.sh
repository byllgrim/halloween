qjackctl --start --active-patchbay=halloween_patchbay.xml &

while ! jack_control status
do
    echo waiting for jack
    sleep 1
done

hydrogen --nosplash --song halloween.h2song &
musescore halloween.mscx &
non-mixer halloween_mixer/ &
nohup yoshimi --state=./yoshimi.state > /dev/null &
